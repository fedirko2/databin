# binData #

Запись чтения бинарных данных по схеме

Допустим у нас есть объект с данными:


var obj = {};
    obj.a = 123;    //integer
    obj.b = 'text'; //string
    obj.c = true;   //boolean
    
нам надо его записать в бинарном виде и читать   

делаем схему:

var scheme = []; //массив параметров

    scheme.push({name:'a',type:t_int32}); // name - названия параметра, type - тип
    scheme.push({name:'b',type:t_string});
    scheme.push({name:'c',type:t_bool});
    
запись:
    
var binData = new writer();	
    binData.write_by_schema(obj,scheme);

var Buffer = BinData.getBuffer();//ArrayBuffer

чтения:

var binData = new reader(<Buffer>);
    
var obj = binData.read_by_schema(scheme);//итог - {a:123,b:'text',c:true}


боле сложная схема, параметр массив


var obj = {};  // объект
    obj.a = 123;    //integer
    obj.b = 'text'; //string
    obj.c = true;   //boolean
    obj.list = [1,3,5];//массив
    

var scheme = []; // схема

    scheme.push({name:'a',type:t_int32});
    scheme.push({name:'b',type:t_string});
    scheme.push({name:'c',type:t_bool});
    scheme.push({name:'list',type:t_array,schema:{type:t_int32, type_len: t_Uint16}});
    
    
    
 к новой схеме добавился тип t_array
 задав этот тип надо добавить еще объект schema.
 объект schema содержит параметры массива, такие как 
 type - тип
 type_len - тип записи количества, в type_len можно поставить еще t_const и t_array_end
 
   t_const - фиксированные значения, задается параметром const_value пример:
      schema:{type:t_int32, type_len: t_const, const_value:123}
   
   t_array_end - читает массив до завершения данных
   
   
схема, в параметре объект:

var obj = {};  // объект
    obj.a = 123;    //integer
    obj.b = 'text'; //string
    obj.c = true;   //boolean
    obj.obj = {}; // объект 2
    obj.obj.a = 321;
    obj.obj.b = false;
    
    

var scheme = []; // схема

var scheme_obj = []; 

    scheme_obj.push({name:'a',type:t_int32});
    scheme_obj.push({name:'b',type:t_bool});

    scheme.push({name:'a',type:t_int32});
    scheme.push({name:'b',type:t_string});
    scheme.push({name:'c',type:t_bool});
    
    scheme.push({name:'obj',type:t_schema,schema:scheme_obj}); 
     
   
   
   
список топов:


 t_bool   
 t_Uint8   
 t_int8   
 t_Uint16 
 t_int16  
 t_Uint32 
 t_int32   
 t_float32 
 t_string  
 t_array   
 t_schema  
 t_const   
 t_array_end    
   
