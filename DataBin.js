 var t_bool    = 1;
 var t_Uint8   = 2;
 var t_int8    = 3;
 var t_Uint16  = 4;
 var t_int16   = 5;
 var t_Uint32  = 6;
 var t_int32   = 7;
 var t_float32 = 8;
 var t_string  = 9;
 
 var t_array   = 10;
 
 var t_schema  = 11;
 
 var t_const   = 12; 
 
 var t_array_end = 13;
 
 
 
 function reader(data){	
 var self = this;
 
 this.offset = 0;
 this.buf = new DataView(data);
 this.length = this.buf.byteLength;
 

  this.readBool = function(){
 
 return this.readInt8() === 1;
 }	
 
 
 this.readUint8 = function(){
 var l = 1;
 if (this.length < this.offset + l) return 0;	
 var val = this.buf.getUint8(this.offset);
 this.offset += l;
 return val;
 }	
 
 this.readInt8 = function(){
 var l = 1;
 if (this.length < this.offset + l) return 0;	
 var val = this.buf.getInt8(this.offset);
 this.offset += l;
 return val;
 }	
 
 
 this.readUint16 = function(){
 var l = 2;
 if (this.length < this.offset + l) return 0;			
 var val = this.buf.getUint16(this.offset);
 this.offset += l;
 return val;
 }	
 
 this.readInt16 = function(){
 var l = 2;
 if (this.length < this.offset + l) return 0;			
 var val = this.buf.getInt16(this.offset);
 this.offset += l;
 return val;
 }
 
 	
 	
 this.readUint32 = function(){
 var l = 4;
 if (this.length < this.offset + l) return 0;	 	
 var val = this.buf.getUint32(this.offset);
 this.offset += l;
 return val;
 }	
 
 this.readInt32 = function(){
 var l = 4;
 if (this.length < this.offset + l) return 0;			
 var val = this.buf.getInt32(this.offset);
 this.offset += l;
 return val;
 }	
 
 
 this.readFloat32 = function(){
 var l = 4;
 if (this.length < this.offset + l) return 0;		
 var val = this.buf.getFloat32(this.offset);
 this.offset += l;
 return val;
 }	
 
 
 this.readString = function(){
 var len =  this.readUint16();
 var str = '';
 for (var i = 0; i < len; i++){
 if (this.length < this.offset + 2) break;	
 var c = this.readUint16();
 str += String.fromCharCode(c);
 }

 return str;
 }	
 
 
 this.rTypes = {};
 
 
 this.rTypes[t_bool]    = function(schema){return self.readBool();}
 this.rTypes[t_Uint8]   = function(schema){return self.readUint8();}
 this.rTypes[t_int8]    = function(schema){return self.readInt8();}
 this.rTypes[t_Uint16]  = function(schema){return self.readUint16();}
 this.rTypes[t_int16]   = function(schema){return self.readInt16();}
 this.rTypes[t_Uint32]  = function(schema){return self.readUint32();}
 this.rTypes[t_int32]   = function(schema){return self.readInt32();}
 this.rTypes[t_float32] = function(schema){return self.readFloat32();}
 this.rTypes[t_string]  = function(schema){return self.readString();}
 
 
 this.rTypes[t_array]  = function(schema){
 	
 	var nodes = schema.schema;
 	
 	
 	var len_arr = self.rTypes[nodes.type_len](nodes);
 	
 	var list = [];
	
	  for (var i = 0; i < len_arr; i++){
		list.push(self.rTypes[nodes.type](nodes));
		}
	 	
 	return list;
 	
 	}
 	
   this.rTypes[t_array_end]  = function(schema){
 	
 	var nodes = schema.schema;
 	
 	var list = [];
 	
 	while (self.length - 1 > self.offset){
 		
 		list.push(self.rTypes[nodes.type](nodes));
 		
 		}
 	
    return list;
 	
 	}	
 	
 	
 
 
  this.rTypes[t_schema]  = function(schema){
  	
  	
  	var obj = {};
  	
  	 var nodes = schema.schema;
  	
  	 for (var i = 0, len = nodes.length; i < len; i++)
 	 {
 	 var node =  nodes[i];
 	 
 	 obj[node.name] = self.rTypes[node.type](node);	
 	 
     }
     
     return obj;
  	
  	
  	}
  	
  	
  	this.rTypes[t_const]  = function(schema){
  	
  	return schema.const_value;	
  		
  	}

 
 

this.read_by_schema = function(schema)
 {
 	
  return this.rTypes[t_schema]({schema:schema});
  
 }
 
 
 
 	
 	
 	
 }
 
 function writer(size){
 	
 var self = this;	
 
 this.list = [];
 this.offset = 0;
 
 this.writeBool = function(val){
 var pak = {};
 	 pak.data = val ? 1 : 0;
 	 pak.type = t_bool;
 	 
 this.list.push(pak);	
 this.offset += 1;
 }
 
 
     this.writeUint8 = function(val){
 	 var pak = {};
 	 pak.data = val;
 	 pak.type = t_Uint8;
 	 
     this.list.push(pak);	
     this.offset += 1;
 	}
 
 
     this.writeInt8 = function(val){
 	 var pak = {};
 	 pak.data = val;
 	 pak.type = t_int8;
 	 
     this.list.push(pak);	
     this.offset += 1;
 	}
 	
 	
 	 this.writeUint16 = function(val){
 	 var pak = {};
 	 pak.data = val;
 	 pak.type = t_Uint16;
 	 
     this.list.push(pak);	
     this.offset += 2;
 	}
 
 
     this.writeInt16 = function(val){
 	 var pak = {};
 	 pak.data = val;
 	 pak.type = t_int16;
 	 
     this.list.push(pak);	
     this.offset += 2;
 	}
 	
 	 this.writeUint32 = function(val){
 	 var pak = {};
 	 pak.data = val;
 	 pak.type = t_Uint32;
 	 
     this.list.push(pak);	
     this.offset += 4;
 	}
 
 
     this.writeInt32 = function(val){
 	 var pak = {};
 	 pak.data = val;
 	 pak.type = t_int32;
 	 
     this.list.push(pak);	
     this.offset += 4;
 	}
 
    this.writeFloat32 = function(val){
 	 var pak = {};
 	 pak.data = val;
 	 pak.type = t_float32;
 	 
     this.list.push(pak);	
     this.offset += 4;
 	}
 	
 	this.writeString = function(val){
 		
 	 if (val == undefined) val = '';	
 		
 	 
 	 var strPak = {};
 	 var lenStr = val.length;
 	 
 	 
 	     strPak.len = lenStr;
 	     
 	     var arStr = [];
    	 this.offset += 2;
 	 
 	 
 	  for (var i = 0; i < lenStr; i++) {
           arStr.push(val.charCodeAt(i));
           this.offset += 2;
     }
     
     strPak.arStr = arStr;
     
     var pak = {};
 	 pak.data = strPak;
 	 pak.type = t_string;
     this.list.push(pak);	
     
 	}
 	
 
 	
 	
 	this.getBuffer = function(){
 	if (!size) size = this.offset;
 	
 	if (!this.buf || this.buf.byteLength !== size)
 	{
		
	
	this.buf = new DataView(new ArrayBuffer(size));
	var offsetBuf = 0;
	
	for (var key in this.list)
	{
	var pak = this.list[key];
	
	 switch (pak.type) {
	 	
	case t_bool:
    this.buf.setUint8(offsetBuf,pak.data);
 	offsetBuf += 1;
 	break;
 	
 	case t_Uint8:
 	this.buf.setUint8(offsetBuf,pak.data);
 	offsetBuf += 1;
    break;
 	
 	case t_int8:
    this.buf.setInt8(offsetBuf,pak.data);	
    offsetBuf += 1;
 	break;
 	
 	case t_Uint16:
    this.buf.setUint16(offsetBuf,pak.data);
    offsetBuf += 2;	
 	break;
 	
 	case t_int16:
    this.buf.setInt16(offsetBuf,pak.data);
    offsetBuf += 2;	
 	break;
 	
 	case t_Uint32:
    this.buf.setUint32(offsetBuf,pak.data);
    offsetBuf += 4;	
 	break;
 	
 	case t_int32:
    this.buf.setUint32(offsetBuf,pak.data);
    offsetBuf += 4;		
 	break;
 	
 	case t_float32:
    this.buf.setFloat32(offsetBuf,pak.data);
    offsetBuf += 4;	
 	break;
 	
 	case t_string:
 	this.buf.setUint16(offsetBuf,pak.data.len);
 	offsetBuf += 2;	
 	
 	var arStr = pak.data.arStr;
 		
 	for (var key in arStr){
	this.buf.setUint16(offsetBuf,arStr[key]);
	offsetBuf += 2;		
	}
 	break;
 	

 }}}
	
	
	return this.buf.buffer;	
		
	}
	
 this.wTypes = {};	

 this.wTypes[t_bool]    = function(value,schema){self.writeBool(value);}
 this.wTypes[t_Uint8]   = function(value,schema){self.writeUint8(value);}
 this.wTypes[t_int8]    = function(value,schema){self.writeInt8(value);}
 this.wTypes[t_Uint16]  = function(value,schema){self.writeUint16(value);}
 this.wTypes[t_int16]   = function(value,schema){self.writeInt16(value);}
 this.wTypes[t_Uint32]  = function(value,schema){self.writeUint32(value);}
 this.wTypes[t_int32]   = function(value,schema){self.writeInt32(value);}
 this.wTypes[t_float32] = function(value,schema){self.writeFloat32(value);}
 this.wTypes[t_string]  = function(value,schema){self.writeString(value);}
 
 this.wTypes[t_array]   = function(value,schema){
 	
 var nodes = schema.schema;	
 
 var len = value.length;
 
 self.wTypes[nodes.type_len](len);	
 
for (var i = 0; i < len; i++){
 self.wTypes[nodes.type](value[i],nodes);	
 }	
 	
 }
 
 this.wTypes[t_array_end] = function(value,schema){
 	
 var nodes = schema.schema;	
 
 var len = value.length;
 
 for (var i = 0; i < len; i++){
 self.wTypes[nodes.type](value[i],nodes);	
 }	
 	
 }
 

 
 
 this.wTypes[t_schema]   = function(value,schema){
 	
 	var nodes = schema.schema;
 	
 	 for (var i = 0, len = nodes.length; i < len; i++)
 	{
 	 var node =  nodes[i];	
 	 self.wTypes[node.type](value[node.name],node);	
		
	}
 }
 
 
 
 this.wTypes[t_const]  = function(value,schema){}
 
 

 	
 this.write_by_schema = function(obj,schema)
 {
 	
   this.wTypes[t_schema](obj,{schema:schema});
  
 }
 	
 	
 	
 		
 	
 }